import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import svgicon from 'vue-svgicon'

Vue.config.productionTip = false;

// Default tag name is 'svgicon'
Vue.use(svgicon, {
    tagName: 'icon'
});

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
