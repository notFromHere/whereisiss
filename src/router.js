import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Nasa.vue'
import WhereIsISS from './views/WhereIsISS.vue'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/whereIsISS',
            name: 'WhereIsISS',
            component: WhereIsISS
        }
    ]
})
